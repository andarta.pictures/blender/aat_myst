# AAT_Myst



## Getting started

### Operator 'Add myst layer':
 - add a tint super modifier that control the myst on each linked grease pencil

### Myst Layer:
 - On/off button
 - name display
 - factor: set ratio
 - color: set color
 - Add link: folding menu of options to link/unlink gp to this Layer
      - ALL
      - SELECTED
      - COLLECTION
      - CLEAR ALL: Unlink all the currently linked gp
      - Or pick a specific GreasePencil in the list of scene gps
  - Linked GP: Folding menu of all linked gps

## TODO:
    FUNC to add:
    - Add mesh support (FTM only Greasepencil are modified)
    - add possibility to remove layers
        
    

## UPDATE LOG

  - v 0.0.2:

    -DONE automyst operator:  add Front or Back color myst on every object that arent in selected collection, depending on there Z positions compared to selected.

    -DONE add selection options: From collection/from selection 
      - Collection: based on outliner last selected collection. apply myst to every object that isn't child of this collection. Limited to one collection
      - Object: Same as collection, but get the collection holding the object currently selected. Limited to one object
          
    - SOLVED sometimes modifiers seem not to unlink properly on automyst recalc
    

  - v 0.0.1:
      - Initial PoC