# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



from .common.import_utils import ensure_lib


bl_info = {
    "name" : "AAT_Myst",
    "author" : "Andarta",
    "description" : "Add and control tint modifier to multiple grease pencil layers",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 2 ),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}

# optional_lib= [
#     ['PIL','pillow'],
#     ]
# for lib in optional_lib :
#     has_lib = ensure_lib(lib[0],lib[1])
#     if not has_lib:
#         print("Warning: Could not import nor install " + lib[0] +'\n'
#               + 'Some features may not work properly \n' +              
#                 'Try again and if this error persist install dependencies manually')

from . import (
    ops,
    ui,
)


modules = [
    ops, 
    ui, 
]

def register():
    for m in modules :
        m.register()

def unregister():
    for m in modules :
        m.unregister()