import bpy

###PSPEUDO CLASSES

import math
from mathutils import Vector

class PseudoVector:
    def __init__(self, data):
        self.data = list(data)

    def __repr__(self):
        return f'PseudoVector({self.data})'

    def __str__(self):
        return f'PseudoVector({self.data})'

    def __eq__(self, other):
        return self.data == other.data

    def __ne__(self, other):
        return not self.__eq__(other)

    def __add__(self, other):
        return PseudoVector([a + b for a, b in zip(self.data, other.data)])

    def __sub__(self, other):
        return PseudoVector([a - b for a, b in zip(self.data, other.data)])

    def __mul__(self, other):
        if isinstance(other, PseudoVector):
            return sum(a * b for a, b in zip(self.data, other.data))
        else:
            return PseudoVector([a * other for a in self.data])

    def __rmul__(self, other):
        return self.__mul__(other)

    def __neg__(self):
        return PseudoVector([-a for a in self.data])

    def __getitem__(self, index):
        return self.data[index]

    def __setitem__(self, index, value):
        self.data[index] = value

    def __len__(self):
        return len(self.data)

    @property
    def length(self):
        return math.sqrt(sum(a**2 for a in self.data))

    def normalized(self):
        length = self.length
        return PseudoVector([a / length for a in self.data])

    @property
    def x(self):
        return self.data[0]

    @x.setter
    def x(self, value):
        self.data[0] = value

    @property
    def y(self):
        return self.data[1]

    @y.setter
    def y(self, value):
        self.data[1] = value

    @property
    def z(self):
        return self.data[2]

    @z.setter
    def z(self, value):
        self.data[2] = value

    @property
    def xyz(self):
        return self.data

    @xyz.setter
    def xyz(self, values):
        self.data = list(values)

class Pseudo_point:
    """store point data without it being a blender point"""
    co=None
    pressure=None
    select=None
    strength=None
    uv_factor=None
    uv_fill=None
    uv_rotation=None
    #vertex_color=None #TODO : it is a bpy_prop_collection need custom pseudo class
    def __init__(self, src_pt=None):
        if src_pt is not None:
            point_confo(src_pt, self, pseudo=True)

    def __repr__(self):
        return 'pseudo_point(%s, %s, %s)'%(self.co, self.pressure, self.strength)

    def __str__(self):
        return 'pseudo_point(%s, %s, %s)'%(self.co, self.pressure, self.strength)

    def __eq__(self, other):
        if isinstance(other, Pseudo_point):
            return self.co == other.co and self.pressure == other.pressure and self.strength == other.strength
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.co, self.pressure, self.strength))

class Pseudo_points:
    points = []
    def __init__(self, src_points=[]):
        self.points = []
        for p in src_points:
            self.points.append(Pseudo_point(p))

    def __iter__(self):
        for p in self.points:
            yield p

    def __getitem__(self, item):
        return self.points[item]
    
    def __len__(self):
        return len(self.points)
    
    def __hash__(self):
        return hash(tuple(self.points))
    
    def new(self):
        self.points.append(Pseudo_point())

    def add(self, count=1):
        for i in range(count):
            self.points.append(Pseudo_point())

    def remove(self, point):
        try:
            self.points.remove(point)
        except:
            pass

class Pseudo_stroke:
    """store stroke data without it being a blender stroke"""
    aspect=None
    display_mode=None
    end_cap_mode=None
    hardness=None
    is_nofill_stroke=None
    line_width=None
    material_index=None    
    select=None
    select_index=None
    start_cap_mode=None
    use_cyclic=None
    uv_rotation=None
    uv_scale=None
    uv_translation=None
    vertex_color_fill=None
    points: Pseudo_points

    def __init__(self, src_stroke=None):
        self.points = Pseudo_points()
        if src_stroke is not None:
            stroke_confo(src_stroke, self)
    def __str__(self) -> str:
        return 'pseudo_stroke(%s, %s, %s)'%(self.points, self.line_width, self.material_index)
        pass

class Pseudo_Strokes:
    strokes = []
    def __init__(self, src_strokes=[]):
        self.strokes = []
        for s in src_strokes:
            self.strokes.append(Pseudo_stroke(s))

    def __iter__(self):
        for s in self.strokes:
            yield s
            
    def __len__(self):
        return len(self.strokes)
    
    def __getitem__(self, item):
        return self.strokes[item]
    
    def new(self):
        new_stroke = Pseudo_stroke()
        self.strokes.append(new_stroke)
        return new_stroke

    def remove(self, stroke):
        try:
            self.strokes.remove(stroke)
        except:
            pass

class Pseudo_gp_frame:
    '''store gp frame data without it being a blender gp frame'''
    strokes : Pseudo_Strokes
    keyframe_type=None
    frame_number=None
    keyframe_type = 'KEYFRAME'
    def __init__(self, src_gpframe):
       self.strokes = Pseudo_Strokes()
       self.frame_number = src_gpframe.frame_number
       frame_confo(src_gpframe, self, pseudo=True)
    
class Pseudo_frames:
    frames = []
    def __init__(self, src_frames=[]):
        self.frames = []
        for f in src_frames:
            self.frames.append(Pseudo_gp_frame(f))

    def __iter__(self):
        for f in self.frames:
            yield f
            
    def __len__(self):
        return len(self.frames)
    
    def __getitem__(self, item):
        return self.frames[item]
    
    def new(self):
        new_frame = Pseudo_gp_frame()
        self.frames.append(new_frame)
        return new_frame

    def remove(self, frame):
        try:
            self.frames.remove(frame)
        except:
            pass
    def add(self, frame):
        self.frames.append(frame)

class Pseudo_gp_layer:
    '''store gp layer data without it being a blender gp layer'''
    frames : Pseudo_frames
    info=None
    opacity=None
    parent=None
    parent_type=None
    show_fill=None
    show_stroke=None
    show_points=None

    def __init__(self,src_gplayer) -> None:
        self.frames = Pseudo_frames()
        for f in src_gplayer.frames:
            self.frames.add(Pseudo_gp_frame(f))

### GENERAL
            
def get_obj_from_data(data):
    gp_object = None
    for obj in bpy.context.scene.objects:
        if obj.data == data:
            gp_object = obj
            return gp_object
    return None
def get_layer_index(layer) :
    '''get the index of a given layer
    args : layer = the given layer
    returns : the index as int'''   
    for i, l in enumerate(layer.id_data.layers) :
        if l == layer :
            return i

def get_now_gpframe(current_frame, layer):
    for frame in layer.frames :
        if frame.frame_number == current_frame :
            return frame
    return None

def get_gplyr_from_name(gp, name):
    '''get a layer from a grease pencil by its name'''
    for tgt_layer in gp.data.layers:
        if name == tgt_layer.info:
            return tgt_layer   
    return None     

def get_gplyr_from_stroke(stroke):
    '''get the gplayer of a given stroke'''
    for layer in bpy.data.grease_pencil[stroke.grease_pencil].layers:
        for s in layer.strokes:
            if s == stroke:
                return layer
      
def move_layer_layer_to_index(layer, index):
    '''moves a layer in the layer's list to give him the specified index
    args : layer = the layer to move
           index = the index to match'''
    layer_index = get_layer_index(layer)
    if layer_index > index :
        for i in range(layer_index - index):
            layer.id_data.layers.move(layer, 'DOWN')
    if layer_index < index :
        for i in range(index - layer_index):
            layer.id_data.layers.move(layer, 'UP')



def sync_layers(src_gp, gp) :
    '''look for missing layers in destination grease pencil (compnewared to a given source object)
    assume that layers haven't been switched compared to source object
    args : src_gp = source grease pencil
           gp = destination grease pencil'''
    new_layers = []
    for i, src_layer in enumerate(src_gp.layers) :
        if src_layer.info not in [layer.info for layer in gp.layers] :
            new_layer = gp.layers.new(src_layer.info)
            #TODO : check layer attributes and relations (parentship, trnasforms, masking relations)
            for attr in dir(src_layer): 
                try:
                    setattr(new_layer, attr, getattr(src_layer, attr)) 
                except:
                    pass
            new_layers.append(new_layer)
            #reorder layer
            if i == 0 :
                move_layer_layer_to_index(new_layer, 0)
            else : 
                previous_layer = src_gp.layers[i-1]
                if previous_layer.info in [layer.info for layer in gp.layers] :
                    before_index = get_layer_index(gp.layers[previous_layer.info])
                    move_layer_layer_to_index(new_layer, before_index + 1)
                else : 
                    move_layer_layer_to_index(new_layer, i)
            if src_layer.parent and src_layer.parent_type == 'OBJECT':  
                try :
                    new_layer.parent = bpy.data.objects[src_layer.parent.name]
                    new_layer.matrix_inverse = src_layer.matrix_inverse
                except :
                    pass

def add_gp_modifier(gp,name = 'GP_SuperSimplifyModifier',mod_type = 'GP_SIMPLIFY' ):
    '''add a grease pencil modifier to a grease pencil object'''
    modifier = gp.grease_pencil_modifiers.new(name=name,type = mod_type)
    return modifier

def get_gp_dic(gp):
    '''return a dictionary of all the frames of all the layers of a gp object as a pseudo gp object'''
    layer_dic ={}
    for layer in gp.layers :
        layer_dic[layer.info] = Pseudo_gp_layer(layer)

    return layer_dic



### ANIMATION
def move_gp_frame(layer, frame, frame_number,update = True):
    '''move a gp frame along the timeline
    the main issue of this method it that every frame of the layer are copies, and direct references to them will now lead to an error
    args : layer = layer whare to move the keyframe
           frame = frame to move
           frame_number = where to move the frame
    returns : the moved frame (note that references to other frames will be corrupted)
    '''
    temp_layer = frame.id_data.layers.new('temp')
    for src_frame in layer.frames : 
        temp_fr = temp_layer.frames.copy(src_frame)
        if src_frame == frame :
            temp_fr.frame_number = frame_number      
    d = dict()    
    for i, temp_fr in enumerate(temp_layer.frames) :
        d[temp_fr.frame_number] = i
    layer.clear()  
    for key in sorted(d) :
        i = d[key]
        new_fr = layer.frames.copy(temp_layer.frames[i])
        if new_fr.frame_number == frame_number :
            moved_frame = new_fr  
    layer.id_data.layers.remove(temp_layer) 
    #still need to kind of refresh by frame change
    if update:
        update_gp_frame_display()
    return moved_frame

def update_gp_frame_display():
        bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
        bpy.context.scene.frame_set(bpy.context.scene.frame_current -1)    
        
def sculpted_frames(frame_1, frame_2):
    '''compare 2 frames to know if they can be interpolated (same amount of strokes and points in every stroke)
    args : frame_1 and frame_2 are the GPencil frames you want to compare
    returns : 'HOLD' if the frames are exactly the same
              'SCULPTED' if the frames are sculpted from each other and should be interpolated
              'DIFFERENT' if they don't have the same amount of strokes/points
    '''
    if len(frame_1.strokes) == len(frame_2.strokes):
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            if len(stroke_1.points) != len(stroke_2.points) :
                return 'DIFFERENT' #not the same amount of points in a stroke
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            for j, point_1 in enumerate(stroke_1.points) :
                point_2 = stroke_2.points[j]
                if point_1.co != point_2.co :
                    return 'SCULPTED'
        return 'HOLD' #not sculpted but exactly the same
    else :
        return 'DIFFERENT'

def get_interpolate_grid_value(frames):
    '''determines if a group of Gpencil frames (usually 4) should be interpolated
    args : frames = a list of frames
    returns : True or False denpending on the result'''
    result_list=[]
    for frame_1 in frames :
        for frame_2 in frames :
            if frame_1 != frame_2 :
                result_list.append(sculpted_frames(frame_1, frame_2))
    if 'DIFFERENT' in result_list :
        return False #can't interpolate with different frames
    else:
        if 'SCULPTED' not in result_list :
            return False #every frames are the same, the layer stays still, no need to interpolate
        else : 
            return True

### MATERIAL
def sync_materials(src_ob, new_ob, verbose = False):
    '''make sure that a new Grease Pencil object has all the materials imported from a source object
    args : src_ob = source object, Grease pencil you're importing from
           new_ob = destination object, grease pencil you're importing on'''
    for src_mat in [mat for mat in src_ob.data.materials if mat is not None] :
        if src_mat.library and src_mat.library == src_ob.library : #template local material 
            if src_mat.name not in [mat.name for mat in new_ob.data.materials if mat is not None]:
                if src_mat.name not in [mat.name for mat in bpy.data.materials if mat.library == None]:
                    new_mat = src_mat.copy()
                    new_mat.name = src_mat.name 
                    if verbose : print('new material created : ', new_mat.name)
                else :
                    new_mat = bpy.data.materials[src_mat.name, None]
                    if verbose : print('material already exists : ', new_mat.name)
                new_ob.data.materials.append(new_mat) 
        elif src_mat.library and src_mat.library != src_ob.library : #template linked material
            if src_mat.name not in [mat.name for mat in new_ob.data.materials if mat is not None] :
                if verbose : print('material %s not found in %s, trying to link it' % (src_mat.name,str(new_ob.data.materials)))
                new_ob.data.materials.append(src_mat)
            if verbose : print('material %s is already linked' % src_mat.name)
        
def frame_confo(src, tgt, keep=False,pseudo=False,call_update=False, verbose = False):
    '''Conform a frame to another without using the blender copy method'''
    #keyframe_type
    tgt.keyframe_type = src.keyframe_type
    i=0
    for src_stroke in src.strokes :
        #search if existing stroke else create new
        if keep and len(tgt.strokes) > i :
            tgt_stroke = tgt.strokes[i]
            i+=1
        else:
            tgt_stroke = tgt.strokes.new()
        stroke_confo(src_stroke, tgt_stroke, keep=keep, pseudo=pseudo,call_update=call_update, verbose=verbose)
    if keep :
        for stroke in tgt.strokes[i:] :
            tgt.strokes.remove(stroke)
    return tgt

def stroke_confo(src, tgt,keep=False, pseudo=False, verbose = False,call_update = True,
                  attributes = ['aspect',
                                'display_mode',
                                'end_cap_mode',
                                'hardness',
                                'is_nofill_stroke',
                                'line_width',
                                'material_index',
                                'select',
                                'select_index',
                                'start_cap_mode',
                                'use_cyclic',
                                'uv_rotation',
                                'uv_scale',
                                'uv_translation',
                                #'vertex_color_fill',
                                ]):
    '''Conform a stroke to another without using the blender copy method''' 
    for a in attributes :
        match a :
            # case 'force_constant_interpolation':
            #     #set frame interpolation to constant
            #     tgt.interpolation = 'CONSTANT'
            case _:
                try:
                    cible_val = getattr(src, a)
                    if type(cible_val) == Vector:
                        cible_val = vector_confo(cible_val, tgt, pseudo=pseudo)
                    #if it is not a python base type
                    elif type(cible_val) not in [int, float, str, bool, Vector, PseudoVector]:
                        if pseudo:
                            continue
                    setattr(tgt, a, cible_val)
                except:
                    print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )         

    if keep:
        count = len(tgt.points)
        if count < len(src.points):
            tgt.points.add(count = len(src.points)-count)
        elif count > len(src.points):
            for i in range(count-len(src.points)):
                tgt.points.pop()
    else:
        tgt.points.add(count = len(src.points))

    for i,point in enumerate(src.points):        
        point_confo(src.points[i], tgt.points[i], pseudo=pseudo, verbose=verbose)
    
    if not pseudo and call_update:
        tgt.points.update()

def point_confo(src, tgt, attributes=['co',
                                    'pressure',
                                    'select',
                                    'strength',
                                    'uv_factor',
                                    'uv_fill',
                                    'uv_rotation',
                                    # 'vertex_color'
                                    ],
                pseudo=False,
                verbose = False):
    '''Conform a point to another without using the blender copy method'''    
    for a in attributes :
        match a :
            # case 'force_constant_interpolation':
            #     #set frame interpolation to constant
            #     tgt.interpolation = 'CONSTANT'
            # case 'co'| ' uv_fill':
            #     #check if source is pseudo or mathutils.Vector
            #     if type(src.co)==PseudoVector:
            #         #convert to mathutils.Vector
            #         if pseudo:
            #             tgt.co = src.co
            #         else:
            #             tgt.co = Vector(tuple(src.co.data))
            #     else:      
            #         if pseudo:
            #             #get Vecotr as python base type
            #             tgt.co = PseudoVector(src.co)
            #         else:
            #             tgt.co = src.co.copy()
            
            case _:
                try:
                    cible_val = getattr(src, a)
                    if type(cible_val) == Vector:
                        cible_val = vector_confo(cible_val, tgt, pseudo=pseudo)
                    elif type(cible_val) not in [int, float, str, bool, Vector, PseudoVector]:
                        if pseudo:
                            continue
                    setattr(tgt, a, cible_val)
                except:
                    print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )        

def vector_confo(src, tgt, pseudo=False):
    '''Conform a vector to another without using the blender copy method'''    
    if type(src)==PseudoVector:
        #convert to mathutils.Vector
        if pseudo:
            tgt = src
        else:
            tgt = Vector(tuple(src.data))
    else:      
        if pseudo:
            #get Vecotr as python base type
            tgt = PseudoVector(src)
        else:
            tgt = src.copy()
    return tgt

def colormatch(src_ob, src_frame, ob, new_frame ) :
    '''Checks if check if the stroke uses same material as source stroke 
    assumes that new_frame is a fresh copy of src frame, but in a different object
    assumes that destination object has all the needed materials already in his list
    args : src_ob = source object
           src_frame = src Gpencil frame 
           ob = destination object
           new_frame = destination gpencil frame'''
    #start_time = time.time()
    matched_strokes = []
    if len(new_frame.strokes) > 0 and len(src_frame.strokes) > 0 :
        for i, stroke in enumerate(new_frame.strokes):
            #stroke_mat = gp_ob.materials[stroke.material_index] #WRONG : doesn't work if there are empty slots
            
            try :
                src_stroke = src_frame.strokes[i] # TOCHECK: why is src_frame.strokes not the same length as new_frame.strokes ?
                stroke_mat = ob.material_slots[stroke.material_index].material
            except IndexError :
                stroke_mat = None

            #src_stroke_mat = src_gp_ob.materials[src_stroke.material_index]
            if len(src_ob.material_slots) > 0 :
                src_stroke_mat = src_ob.material_slots[src_stroke.material_index].material
            else :
                src_stroke_mat = None

            if src_stroke_mat :
                if stroke_mat == None or stroke_mat.name != src_stroke_mat.name :
                    #print('color dont match ! ' + src_stroke_mat.name + ' - ' + stroke_mat.name)
                    matched_strokes.append(stroke)

                    for index, material_slot in enumerate(ob.material_slots) :
                        material = material_slot.material
                        if material is not None :
                            if material.name == src_stroke_mat.name or material.name == src_stroke_mat.name :
                                stroke.material_index = index
                                #print(str(i) + ' - colormatching : ' + src_stroke_mat.name + ' - ' +  material.name )
            if src_stroke_mat == None :
                stroke.material_index = -1
    #print('COLORMATCH : relocated ' + str(len(matched_strokes)) + ' strokes material : ', time.time() - start_time )                    
    return {'FINISHED'}
