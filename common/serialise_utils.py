import base64
import gzip
import pickle
import bpy

def convert_prop_array(data):
    '''convert a bpy.types.bpy_prop_array to a list'''
    if isinstance(data, bpy.types.bpy_prop_array):
        return list(data)
    elif isinstance(data, dict):
        return {k: convert_prop_array(v) for k, v in data.items()}
    elif isinstance(data, list):
        return [convert_prop_array(v) for v in data]
    else:
        return data
    
def serialize_blender_pseudo_object(data, compress=True):
    '''convert a blender pseudo object to a string'''
    # obj = convert_prop_array(obj)
    pickled_data = pickle.dumps(data)
    if compress:
        pickled_data = gzip.compress(pickled_data)
    return base64.b64encode(pickled_data).decode('utf-8')

def deserialize_blender_pseudo_object(data, compressed=True):
    '''convert a serialized pseudo object string to a blender pseudo object'''
    data = base64.b64decode(data.encode('utf-8'))
    if compressed:
        data = gzip.decompress(data)
    return pickle.loads(data)
