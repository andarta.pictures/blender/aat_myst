import bpy
from bpy_extras.view3d_utils import location_3d_to_region_2d
import gpu
from gpu_extras.batch import batch_for_shader

'''
https://blender.stackexchange.com/questions/6377/coordinates-of-corners-of-camera-view-border
Thanks to ideasman42
'''
def view3d_find():
    # returns first 3d view, normally we get from context
    for area in bpy.context.window.screen.areas:
        if area.type == 'VIEW_3D':
            v3d = area.spaces[0]
            rv3d = v3d.region_3d
            for region in area.regions:
                if region.type == 'WINDOW':
                    return region, rv3d
    return None, None

def view3d_camera_border(scene):
    obj = scene.camera
    cam = obj.data

    frame = cam.view_frame(scene=scene)

    # move from object-space into world-space 
    frame = [obj.matrix_world @ v for v in frame]

    # move into pixelspace   
    region, rv3d = view3d_find()
    frame_px = [location_3d_to_region_2d(region, rv3d, v) for v in frame]
    return frame_px,frame

def object_to_collection (obj,cible_name = 'unknown'):

    for c in  obj.users_collection:
        c.objects.unlink(obj)
        
    if (bpy.data.collections.find(cible_name) == True):
        collection_cible = bpy.data.collections[cible_name]
    else:
        collection_cible = bpy.data.collections.new(cible_name)  
        bpy.context.scene.collection.children.link(collection_cible)

    collection_cible.objects.link(obj)

