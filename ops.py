
import bpy
from .common.general_utils import get_object_collections
from .common.serialise_utils import deserialize_blender_pseudo_object, serialize_blender_pseudo_object
from .common.gp_utils import Pseudo_gp_frame, Pseudo_gp_layer, add_gp_modifier, frame_confo, get_gp_dic, get_gplyr_from_name, get_now_gpframe, get_obj_from_data
from .common.utils import register_classes, unregister_classes
from .common.gp_utils_bis import get_grease_pencil_layer,get_grease_pencil
from .common.utils_bis import object_to_collection, view3d_camera_border
import mathutils
from bpy.app.handlers import persistent




''' CORE DEF'''
def get_myst_layer(modifier_name):
    collection = bpy.context.scene.myst_collection
    for pot in collection:
        if pot.modifier_name == modifier_name:
            return pot
            

def update_linked_gp_modifier(self,context):
    '''update all the linked gp modifier (adapted to Tint)'''
    
    for gp in bpy.context.scene.objects:
        if gp.type == 'GPENCIL':        
            if gp is None or not gp.data.myst_gp_data.is_linked:
                continue
            prop_name =  gp.data.myst_gp_data.modifier_name
            prop = get_myst_layer(prop_name)
            
            # gp.myst_gp_data.is_linked = True
            modifier = gp.grease_pencil_modifiers.get(prop.modifier_name)
            if modifier is None:
                modifier = add_gp_modifier(gp, 
                                           prop.modifier_name,
                                           mod_type = 'GP_TINT')
            modifier.show_viewport = prop.show_viewport            
            modifier.color = prop.color
            modifier.factor = prop.factor

 
def unlink_gp_modifier(gp,modifier_name):
    modifier = gp.grease_pencil_modifiers.get(modifier_name)
    if modifier is not None:
        gp.grease_pencil_modifiers.remove(modifier)

class AAT_MYST_addon_prefs(bpy.types.AddonPreferences) :
    bl_idname = __package__
    front_color : bpy.props.FloatVectorProperty(
        name="Front color",
        subtype='COLOR',
        default =(0.93, 0.03, 0.22),
        min=0.0, max=1.0,
        description="auto myst front color",
        update = update_linked_gp_modifier
        )
    
    back_color : bpy.props.FloatVectorProperty(
        name="Back color",
        subtype='COLOR',
        default =(0.042, 0.42, 0.42),
        min=0.0, max=1.0,
        description="auto myst back color",
        update = update_linked_gp_modifier
        )
    selection_mode : bpy.props.EnumProperty(items = [('COL','Collection','Collection'), 
                                           ('OBJ','Object','Object'), 
                                           ]
                                           , default = 'COL'
                                           )


    def draw(self, context) :
        layout = self.layout
        layout.label(text = "AAT Myst Addon Preferences", icon = "MOD_GPENCIL")
        layout.prop(self, 'front_color')
        layout.prop(self, 'back_color')

def get_z_depth(object):
    '''get z depth of the gp'''
    #get z position from camera which is equal to sqrt((x - xcam)² + (y - ycam)² + (z - zcam)²)
    # Get the camera and object location vectors
    # cam_loc = bpy.context.scene.camera.location
    # obj_loc = object.location
    camera = bpy.context.scene.camera
    if not camera:
        return 0 
    
    cam_loc = camera.matrix_world.translation
    obj_loc = object.matrix_world.translation

    # Calculate the vector from the camera to the object
    vec = obj_loc - cam_loc

    # Get the camera's direction vector and normalize it
    cam_dir = bpy.context.scene.camera.matrix_world.to_quaternion() @ mathutils.Vector((0.0, 0.0, -1.0))

    # Project the vector onto the camera direction
    proj = vec.project(cam_dir)

    # The length of the projection is the distance from the camera to the object along the camera's view axis
    distance = proj.length
    return distance

class AAT_MYST_auto_myst(bpy.types.Operator) :
    '''Add a myst modifier to all the grease pencil accept those selected and their children
       Based on z postion compared to selected, apply front or back color to modifier'''
    bl_idname = "gp.auto_myst"
    bl_label = "Auto Myst"
    bl_description = "Add a myst modifier to all the grease pencil accept those selected and their children"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context) :
        #get addon preferences
        preferences = context.preferences
        addon_prefs = preferences.addons[__package__].preferences
        collections = []
        if addon_prefs.selection_mode == 'COL':
            selected_collection = context.view_layer.active_layer_collection.collection
            #get selected
            objects_dic= selected_collection.all_objects
        elif addon_prefs.selection_mode == 'OBJ':
            cols = {}
            for obj in bpy.context.selected_objects:
                cols = cols|get_object_collections(obj, recursiv_dic = {}, get_elders = False)
            #get first entry of cols
            col = next(iter(cols.values()))    
                     
            objects_dic = col.all_objects

        #get first object of objects_dic depth as reference
        selected_z = get_z_depth(objects_dic[0])

        #get or create super_modifer
        collection = context.scene.myst_collection
        #Front
        front_modifier = None
        back_modifier = None
        for modifier in collection:
            if modifier.modifier_name == 'Front_auto myst':
                front_modifier = modifier
            elif modifier.modifier_name == 'Back_auto myst':
                back_modifier = modifier
        if not front_modifier:
            front_modifier = collection.add()
            front_modifier.modifier_name = 'Front_auto myst'
            front_modifier.color = bpy.context.preferences.addons[__package__].preferences.front_color
        if not back_modifier:
            back_modifier = collection.add()
            back_modifier.modifier_name = 'Back_auto myst'
            back_modifier.color = bpy.context.preferences.addons[__package__].preferences.back_color

        #iterate trough all scene objects
        for obj in bpy.context.scene.objects:
            #check if gp
            if obj.type == 'GPENCIL':
                #check if in selected
                if obj.name in objects_dic:
                    if (obj.data.myst_gp_data.modifier_name == front_modifier.modifier_name 
                        or obj.data.myst_gp_data.modifier_name == back_modifier.modifier_name):
                        
                        obj.data.myst_gp_data.is_linked = False
                    continue
                
                
                #get z position from camera which is equal to sqrt((x - xcam)² + (y - ycam)² + (z - zcam)²)
                tgt_z = get_z_depth(obj)
                

                # z = obj.location.z
                # #get selected z position
                # selected_z = bpy.context.object.location.z
                # #if above selected apply front color else back color
                if obj.data.myst_gp_data.is_linked :
                    obj.data.myst_gp_data.is_linked = False

                if tgt_z < selected_z:
                   obj.data.myst_gp_data.modifier_name = front_modifier.modifier_name    
                else:
                    obj.data.myst_gp_data.modifier_name = back_modifier.modifier_name  
                
                obj.data.myst_gp_data.is_linked = True 
                
        

        return {'FINISHED'}

class AAT_MYST_multiple_gp_modifier(bpy.types.PropertyGroup) :    
    '''Hold the parameters to drive multiple gp tint modifier'''
    bl_label = "TGPR Properties"
    bl_idname = "tgpr_data"

    # mode : bpy.props.EnumProperty(
    #     items = 
    # [
    #         ("FIXED", "Fixed", "Fixed"),
    #         ("ADAPTIVE", "Adaptive", "Adaptive"),
    #     ],
    #     name = "Mode",
    #     default = "ADAPTIVE",
    #     update = update_linked_gp_modifier   
    # )
    factor : bpy.props.FloatProperty(
        name = "factor",
        default = 0.5,
        min = 0.0,
        max = 2.0,
        update = update_linked_gp_modifier
    )
    
    color : bpy.props.FloatVectorProperty(
        name="Color",
        subtype='COLOR',
        default =(0.042, 0.42, 0.42),
        min=0.0, max=1.0,
        description="myst color",
        update = update_linked_gp_modifier
        )
    
    
    show_viewport : bpy.props.BoolProperty(
        name = "Show viewport",
        default = True,
        update = update_linked_gp_modifier
    )

    '''GP LIST MANAGEMENT'''

    selected_gp_names : bpy.props.StringProperty(
        name='Selected GPs',
        description='',
        default='',
        update = update_linked_gp_modifier
    )
    

    def update_driven_gp_list(self,value):
        match value :
            case  'CLEAR ALL' | '':
                for gp in bpy.context.scene.objects:
                    if gp.type == 'GPENCIL':                
                        if (gp.data.myst_gp_data.is_linked 
                            and gp.data.myst_gp_data.modifier_name == self.modifier_name):
                            gp.data.myst_gp_data.is_linked = False
                    
                # self.selected_gp_names = ''
            case 'ADD SELECTED':
                #add name of all selected objects that are gp
                # to_add = ''
                for gp in bpy.context.selected_objects :
                    if isinstance(gp.data, bpy.types.GreasePencil) :  
                        if not gp.data.myst_gp_data.modifier_name == self.modifier_name :
                            gp.data.myst_gp_data.modifier_name = self.modifier_name  
                            gp.data.myst_gp_data.is_linked = True
                                                

            case 'LINK ALL':
                for gp in bpy.context.scene.objects:
                    if gp.type == 'GPENCIL':                
                        if gp.data.myst_gp_data.modifier_name == self.modifier_name:
                            gp.data.myst_gp_data.modifier_name = self.modifier_name
                            gp.data.myst_gp_data.is_linked = True
                            
            case _ :
                gp = bpy.data.objects.get(value) 
                if gp is None:
                    return
                if  gp.data.myst_gp_data.modifier_name == self.modifier_name:
                    gp.data.myst_gp_data.modifier_name = self.modifier_name 
                    gp.myst_gp_data.is_linked = True
                    

    driven_gp_choice: bpy.props.StringProperty(
        name='Add GP to modifier list',
        description='',
        default='',
        search=lambda self, context, edit_text: ['CLEAR ALL',
                                                 'ADD SELECTED',
                                                 'LINK ALL'
                                                 ]+[
                                                gp.name for gp in bpy.context.scene.objects 
                                                if isinstance(gp.data, bpy.types.GreasePencil) 
                                                and gp.data.myst_gp_data.modifier_name!=self.modifier_name
                                                ],
        set = update_driven_gp_list
    )
    '''INDIVIDUATION'''
    modifier_name : bpy.props.StringProperty(
        name = "Modifier name",
        default = "Myst Layer"
    )

    '''UI'''
    expanded : bpy.props.BoolProperty(
        name = "Expanded",
        default = False
    )

    
       

def link_gp(self,context):    
    # if self.is_packed:
    #     self.is_linked = False
    #     return
    # send blender warning
    
    gp_data = self.id_data
    gp_object = get_obj_from_data(gp_data)
    prop_name = self.modifier_name#bpy.context.scene.tgrp_data
    collection = bpy.context.scene.myst_collection
    for pot in collection:
        if pot.modifier_name == prop_name:
            prop = pot
            break
    
    #forbid if packed    
    if self.is_linked:
        modifier = gp_object.grease_pencil_modifiers.get(prop.modifier_name)
        if modifier is None:
            add_gp_modifier(gp_object,name=prop.modifier_name,mod_type='GP_TINT')
    else:
        unlink_gp_modifier(gp_object,prop.modifier_name)
    update_linked_gp_modifier(self,context)

class AAT_MYST_gp_modifier_data(bpy.types.PropertyGroup) :
    '''Hold the parameters to drive multiple gp simplify modifier'''
    bl_label = "TGPR Properties"
    is_linked : bpy.props.BoolProperty(
        name = "Is linked",
        default = False,
        update = link_gp
    )
    modifier_name: bpy.props.StringProperty(
        name = "Modifier name",
        default = ""
    )

# a collection property to hold all the AAT_MYST_multiple_gp_modifier
# class AAT_MYST_gp_modifier_collection(bpy.types.CollectionProperty) :
#     type = AAT_MYST_multiple_gp_modifier

#     #overdie add
#     @classmethod
#     def add(self, item) :
#         super().add()
#         item.modifier = self
#         item.is_linked = True

class AAT_Add_GP_supermodifier(bpy.types.Operator) :
    bl_idname = "gp.supermodifier_add"
    bl_label = "Add GP supermodifier"
    bl_description = "Add a supermodifier to the selected grease pencil"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context) :
        collection = context.scene.myst_collection
        modifier = collection.add()
        modifier.modifier_name +=  ' '+str(len(collection))
        
        return {'FINISHED'}


classes = [
    AAT_MYST_addon_prefs,
    AAT_MYST_auto_myst,
    AAT_MYST_multiple_gp_modifier,
    AAT_MYST_gp_modifier_data,
    AAT_Add_GP_supermodifier
]

def register() :

    register_classes(classes)
    bpy.types.GreasePencil.myst_gp_data = bpy.props.PointerProperty(type = AAT_MYST_gp_modifier_data)
    # bpy.types.Scene.tgrp_data = bpy.props.PointerProperty(type=AAT_MYST_multiple_gp_modifier)
    bpy.types.Scene.myst_collection = bpy.props.CollectionProperty(type = AAT_MYST_multiple_gp_modifier)
    

def unregister() :

    # del bpy.types.Scene.tgrp_data
    del bpy.types.GreasePencil.myst_gp_data
    del bpy.types.Scene.myst_collection
    unregister_classes(classes)
