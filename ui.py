
import bpy

class View_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "AAT_Myst"
    bl_idname = "OBJECT_MYST_viewpanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"


    def draw(self, context):
        row = self.layout.row()
        collection = context.scene.myst_collection
        preferences = context.preferences
        addon_prefs = preferences.addons[__package__].preferences
        row.label(text = 'Auto myst:')  
        row = self.layout.row()
        front_modifier = None
        back_modifier = None
        for modifier in collection:
            if modifier.modifier_name == 'Front_auto myst':
                front_modifier = modifier
            elif modifier.modifier_name == 'Back_auto myst':
                back_modifier = modifier
        if front_modifier:
            row.prop(front_modifier,'color',text = 'Front Color')
        else:
            row.prop(addon_prefs,'front_color',text = "Front Color")

        if back_modifier:
            row.prop(back_modifier,'color',text = 'Back Color')
        else:
            row.prop(addon_prefs,'back_color',text = "Back Color")
        row = self.layout.row()

        row.prop(addon_prefs,'selection_mode',text = "Selection Mode",expand = True)
        # get selected collection
        if addon_prefs.selection_mode == 'COL':
            selected_collection = context.view_layer.active_layer_collection.collection
        elif addon_prefs.selection_mode == 'OBJ':
            if len(context.selected_objects) == 0:
                selected_collection = None
            else:
                selected_collection = context.selected_objects[0]
        row = self.layout.row()
        if not selected_collection:
            row.label(text = 'select an OBJ/COL to generate from!')
        else:            
            row.label(text =  selected_collection.name)
            row = self.layout.row()
            row_operator = row.operator('gp.auto_myst',text = 'Generate myst for selection')
            

        row = self.layout.row()
        #add separator
        self.layout.separator()
        row.label(text = 'Manual myst:')  
        row = self.layout.row()
        row.operator('gp.supermodifier_add',text = 'Add myst layer')
        #add separtor
        if len(collection) == 0:
            row = self.layout.row()
            row.label(text = 'No layer')
            return

        row = self.layout.row()
        row.label(text = 'Layers:')        
        for prop in collection:
        # prop = bpy.context.scene.tgrp_data
            layerbox = self.layout.box()
            row = layerbox.row()
                   
            row.label(text = prop.modifier_name)    
            # row = layerbox.row()
            # row.prop(prop,'mode',text = "Mode")
            # row = layerbox.row()
            # match prop.mode :
            #     case 'FIXED':
            #         row.prop(prop,'iterations',text = "Iterations")           
            #     case 'ADAPTIVE':                                
            #         row.prop(prop,'factor',text = "Factor")                  
            
            row = layerbox.row()
            row.prop(prop,
                     'show_viewport',
                     text = 'On' if prop.show_viewport else 'Off',
                     toggle = True)     
            row.prop(prop,'factor',text = "factor")
            row.prop(prop,'color',text = "")
            
            row = layerbox.row()
            # row.prop_search(prop,'driven_gp_choice',bpy.data,'grease_pencils',text = "ADD GP")
            row.prop(prop,'driven_gp_choice',text = "Add link")
            row = layerbox.row()
            row.prop(prop, "expanded",
                            icon="TRIA_DOWN" if prop.expanded else "TRIA_RIGHT",
                            icon_only=True, emboss=False
                        )
            row.label(text = "Linked GPs:")            
            box = layerbox.box() 

            if prop.expanded:            
                for obj in bpy.context.scene.objects:
                    if obj.type == 'GPENCIL':
                        gp = obj.data
                        if not (gp.myst_gp_data.is_linked and gp.myst_gp_data.modifier_name != prop.modifier_name):
                            continue
                        row = box.row()
                        #add name as label
                        row.label(text = gp.name)

                        row.prop(gp.myst_gp_data,'is_linked',
                                text = 'link' if not gp.myst_gp_data.is_linked else 'X',
                                toggle = True  )
                        # else:
                        #     row.label(text = 'simplified')
                        

        

            
        



def register() :
    bpy.utils.register_class(View_Panel)

def unregister() :
    bpy.utils.unregister_class(View_Panel)
